var express=require("express"),
    handlebars=require('express-handlebars'),
    bodyParser=require('body-parser');

var app = express();

var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
            {
                "subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            }];

app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");

app.get('/posts', function(req, res)
{
    res.render("posts", {
      title:"Posts:",
      post:posts
    } );
});

app.get('/posts/new', function(req,res)
{
  var lastPost=posts[posts.length-1];
  res.render("posts", {
    title:"Last post",
    post:[lastPost]
  } );
})


app.post('/post',function(req,res)
{
  posts.push(res.body);
})


app.get('/posts/:id', function(req,res)
{
  var index=req.params.id;
  var maxIndex=posts.length-1;
  if(index>maxIndex)
    index=maxIndex;

  var post=posts[index];
  res.render("posts", {
    title:"Post: ",
    post:[post]
  } );
})

app.get('/posts/edit', function(req,res)
{
  res.end("Edit last post");
})
app.put('/posts/:id', function(req,res)
{
  res.end("Edit post on id");
})

app.delete('/posts/:id', function(req,res)
{
  var index=req.params.id;
  var maxIndex=posts.length-1;
  if(index>maxIndex)
    index=maxIndex;

  posts.splice(index, 1);
})

app.listen(8080);
